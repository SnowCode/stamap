#!/bin/python3
import staticmaps, geopy, routingpy

client = routingpy.Graphhopper(api_key="91db5de9-bef9-405e-a41c-b83059635619")
geoloc = geopy.Nominatim(user_agent="blablablablebleble")

def get_context():
    context = staticmaps.Context()
    context.set_tile_provider(staticmaps.tile_provider_OSM)
    return context

def get_coords(address):
    location = geoloc.geocode(address)
    return (location.longitude, location.latitude)

def create_marker(context, location, color="red"):
    if type(location) == str:
        location = get_coords(location)

    point = staticmaps.create_latlng(location[1], location[0])
    marker = staticmaps.Marker(point, color=staticmaps.parse_color(color))
    context.add_object(marker)
    return context

def create_trace(context, origin, destination, profile):
    if type(origin) == str:
        origin = get_coords(origin)
    if type(destination) == str:
        destination = get_coords(destination)

    route = client.directions(locations=[origin, destination], profile=profile)
    list = []
    for point in route.geometry:
        list.append(staticmaps.create_latlng(point[1], point[0]))
    line = staticmaps.Line(list)
    context.add_object(line)
    return context
