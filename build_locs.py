#!/bin/python3
from stamap import get_context, create_marker
import sys, csv

if len(sys.argv) <= 2:
    filename = "markers.csv"
    if len(sys.argv) == 1:
        print("No output filename provided")
        exit()
    else:
        output_filename = sys.argv[1]
else:
    filename = sys.argv[1]

c = get_context()

reader = csv.reader(open(filename))
for row in reader:
    coords = (float(row[1]), float(row[2]))
    c = create_marker(c, coords, color=row[3])
    print(row)

c.render_cairo(1920,1080).write_to_png(output_filename)
