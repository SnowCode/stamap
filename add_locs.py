#!/bin/python3
import sys, datetime
from stamap import get_coords

if len(sys.argv) == 1:
    filename = "markers.csv"
else:
    filename = sys.argv[1]

while True:
    address = input("Location> ")
    if address == '':
        print("Empty location, exitting...")
        break
    location = get_coords(address)
    print(location)
    color = input("Color> ")
    entry = f'"{address}",{location[0]},{location[1]},{color}\n'
    open(filename, "a").write(entry)
    print(entry)
