#!/bin/python3
from stamap import get_context, create_marker, create_trace
import sys

if len(sys.argv) == 1:
    print("No output filename provided")
    exit()
else:
    filename = sys.argv[1]

origin = input("Origin> ")
destination = input("Destination> ")
profile = input("Profile> ")

c = get_context()
c = create_marker(c, origin, color="green")
c = create_marker(c, destination, color="red")
c = create_trace(c, origin, destination, profile)

c.render_cairo(1920,1080).write_to_png(filename)

