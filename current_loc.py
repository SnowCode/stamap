#!/bin/python3
from termux.API import location
import sys, datetime

if len(sys.argv) <= 2:
    filename = "markers.csv"
    if len(sys.argv) == 1:
        color = "red"
    else:
        color = sys.argv[1]
else:
    filename = sys.argv[2]

current = location()[1]
time = datetime.datetime.now()

entry = f'"{time}",{current["longitude"]},{current["latitude"]},{color}\n'
open(filename, "a").write(entry)
print(entry)
